# How to run this?

1. Install Supabase CLI <https://supabase.com/docs/guides/cli>
2. Run `supabase start` to run locally. You will see something like this

```
         API URL: http://localhost:54321
     GraphQL URL: http://localhost:54321/graphql/v1
          DB URL: postgresql://postgres:postgres@localhost:54322/postgres
      Studio URL: http://localhost:54323
    Inbucket URL: http://localhost:54324
      JWT secret: super-secret-jwt-token-with-at-least-32-characters-long
        anon key: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJzdXBhYmFzZS1kZW1vIiwicm9sZSI6ImFub24iLCJleHAiOjE5ODM4MTI5OTZ9.CRXP1A7WOeoJeXxjNni43kdQwgnWNReilDMblYTn_I0
service_role key: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJzdXBhYmFzZS1kZW1vIiwicm9sZSI6InNlcnZpY2Vfcm9sZSIsImV4cCI6MTk4MzgxMjk5Nn0.EGIM96RAZx35lJzdJsyH-qQwv8Hdp7fsn3W0YpN81IU
```

3. Run `supabase db reset` to generate the tables
4. Update the value in .env files of `venusai-backend`: Use `API URL` and `service_role key`
5. Update the value in .env files of `venusai-frontend`: : Use `API URL` and `anon key`. Never expose `service_role key` on frontend.
6. Use this guide to migrate to other project or hosted project <https://supabase.com/docs/guides/platform/migrating-and-upgrading-projects>

# Scripts to dump data for backup

- Run `./dump.sh` to dump data
- To dump it in local db, run `./restore.sh postgresql://postgres:postgres@localhost:54322/postgres`
