#!/bin/bash

NEW_DB_URL="$1"

psql \
  --single-transaction \
  --variable ON_ERROR_STOP=1 \
  --file dump/schema.sql \
  --file dump/data.sql \
  --dbname "$NEW_DB_URL"