rm -rf dump
mkdir dump

supabase db dump -f dump/roles.sql --role-only
supabase db dump -f dump/schema.sql
supabase db dump -f dump/data.sql --data-only