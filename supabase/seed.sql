INSERT INTO "public"."tags"
	(id,created_at,name,slug,description)
VALUES
	(27, '2023-04-12 08:58:47.077388+00', '👩🏼‍💻 VTuber', 'vtuber', 'VTuber'),
	(26, '2023-04-04 09:22:10.806356+00', '👭 Multiple', 'multiple_people', 'Character contains more than 1 character'),
	(28, '2023-05-02 16:02:22.442826+00', '⛓️ Dominant', 'dominant', 'BDSM - Dominant character'),
	(29, '2023-05-02 16:03:02.587313+00', '🙇 Submissive', 'submissive', 'BDSM - Submissive character'),
	(31, '2023-05-03 15:09:09.108855+00', '🐙 Pokemon', 'pokemon', 'This tag will attract furry, use it with care'),
	(30, '2023-05-03 15:07:36.338211+00', '🪢 Scenario', 'scenario', 'For scenario/RPG/setup card, not a character'),
	(32, '2023-05-03 15:10:57.527409+00', '👨🏼‍🦲👨🏿‍🦲👱‍♂️ NTR', 'ntr', 'Netorare or Netori character, cuckold in English'),
	(21, '2023-04-04 09:18:30+00', '🫦 Succubus', 'succubus', 'Succubus/Succubi characters'),
	(23, '2023-04-04 09:18:30.194451+00', '🧖🏼‍♀️ Giant', 'giant', 'Giant/Giantess/Big size characters'),
	(33, '2023-05-12 08:18:56.697978+00', '🍆 Futa', 'futa', 'Girl with dick. If you know, you know'),
	(34, '2023-05-13 02:38:08.955747+00', '🌎 Non-English', 'non_english', 'Characters whose main language is not English'),
	(35, '2023-05-13 16:02:12.019557+00', '🧑‍🦳👨‍🦳 Mature', 'mature', 'For MILF, DILF, mommy characters'),
	(36, '2023-05-13 16:03:04.011008+00', '🍑 Femboy', 'femboy', 'Feminine males or traps'),
	(37, '2023-05-18 00:22:18.076899+00', '👪 Family', 'family', 'Character is in the same family as you (sister/brother/mom/dad/cousin...)as'),
	(38, '2023-05-19 18:21:49.600849+00', '🎲 RPG', 'rpg', 'For RPG bots'),
	(1, '2023-04-04 09:18:30.194451+00', '👨‍🦰 Male', 'male', 'Male characters'),
	(2, '2023-04-04 09:18:30.194451+00', '👩‍🦰 Female', 'female', 'Female characters'),
	(3, '2023-04-04 09:18:30.194451+00', '🌈 Non-binary', 'non_binary', 'Non-binary characters'),
	(4, '2023-04-04 09:18:30.194451+00', '🎭 Celebrity', 'celebrity', 'Famous people'),
	(6, '2023-04-04 09:18:30.194451+00', '📚 Fictional', 'fictional', 'Fictional characters'),
	(5, '2023-04-04 09:18:30.194451+00', '🧑‍🎨 OC', 'oc', 'Original characters'),
	(7, '2023-04-04 09:18:30.194451+00', '👤 Real', 'real', 'Real people'),
	(8, '2023-04-04 09:18:30.194451+00', '🎮 Game', 'game', 'Characters originating from video games'),
	(9, '2023-04-04 09:18:30.194451+00', '📺 Anime', 'anime', 'Characters originating from anime or manga'),
	(11, '2023-04-04 09:18:30.194451+00', '👑 Royalty', 'royalty', 'Characters with royal lineage or titles'),
	(10, '2023-04-04 09:18:30.194451+00', '🏰 Historical', 'historical', 'Characters from a historical period or setting'),
	(13, '2023-04-04 09:18:30.194451+00', '🦸‍♂️ Hero', 'hero', 'Heroic characters'),
	(12, '2023-04-04 09:18:30.194451+00', '🕵️‍♀️ Detective', 'detective', 'Characters who work as detectives'),
	(14, '2023-04-04 09:18:30.194451+00', '🦹‍♂️ Villain', 'villain', 'Villainous characters'),
	(15, '2023-04-04 09:18:30.194451+00', '🔮 Magical', 'magical', 'Characters with magical abilities'),
	(16, '2023-04-04 09:18:30.194451+00', '🦄 Non-human', 'non_human', 'Non-human characters'),
	(17, '2023-04-04 09:18:30.194451+00', '👹 Monster', 'monster', 'Monstrous characters'),
	(18, '2023-04-04 09:18:30.194451+00', '👧 Monster Girl', 'monster_girl', 'Female monstrous characters'),
	(20, '2023-04-04 09:18:30.194451+00', '🤖 Robot', 'robot', 'Mechanical or electronic characters'),
	(19, '2023-04-04 09:18:30.194451+00', '👽 Alien', 'alien', 'Characters from other planets'),
	(22, '2023-04-04 09:18:30.194451+00', '🧛‍♂️ Vampire', 'vampire', 'Characters that are vampires'),
	(24, '2023-04-04 09:20:24.617833+00', '🤐 OpenAI', 'oai', 'Character is optimized to work with OpenAI ONLY'),
	(25, '2023-04-04 09:21:11.89822+00', '🧝‍♀️ Elf', 'elf', 'Elf race');

INSERT INTO "storage"."buckets"
	(id,name,owner,created_at,updated_at,"public",avif_autodetection,file_size_limit,allowed_mime_types)
VALUES
	('avatars', 'avatars', NULL, '2023-02-28 05:20:17.181272+00', '2023-02-28 05:20:17.181272+00', 'true', 'false', NULL, NULL),
	('bot-avatars', 'bot-avatars', NULL, '2023-02-28 05:20:33.393196+00', '2023-02-28 05:20:33.393196+00', 'true', 'false', NULL, NULL);
