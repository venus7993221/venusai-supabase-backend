SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

CREATE EXTENSION IF NOT EXISTS "pgsodium" WITH SCHEMA "pgsodium";

ALTER SCHEMA "public" OWNER TO "postgres";

CREATE EXTENSION IF NOT EXISTS "pg_graphql" WITH SCHEMA "graphql";

CREATE EXTENSION IF NOT EXISTS "pg_stat_statements" WITH SCHEMA "extensions";

CREATE EXTENSION IF NOT EXISTS "pgcrypto" WITH SCHEMA "extensions";

CREATE EXTENSION IF NOT EXISTS "pgjwt" WITH SCHEMA "extensions";

CREATE EXTENSION IF NOT EXISTS "uuid-ossp" WITH SCHEMA "extensions";

CREATE FUNCTION "public"."handle_new_user"() RETURNS "trigger"
    LANGUAGE "plpgsql" SECURITY DEFINER
    SET "search_path" TO 'public'
    AS $$
begin
  insert into public.user_profiles (id)
  values (new.id);
  return new;
end;
$$;

ALTER FUNCTION "public"."handle_new_user"() OWNER TO "postgres";

CREATE FUNCTION "public"."pgrst_watch"() RETURNS "void"
    LANGUAGE "plpgsql"
    AS $$
BEGIN
  NOTIFY pgrst, 'reload schema';
END;
$$;

ALTER FUNCTION "public"."pgrst_watch"() OWNER TO "postgres";

CREATE FUNCTION "public"."refresh_materialized_view"("view_name" "text") RETURNS "void"
    LANGUAGE "plpgsql" SECURITY DEFINER
    AS $$
BEGIN
  set statement_timeout to 120000;
  EXECUTE 'REFRESH MATERIALIZED VIEW ' || view_name;
END;
$$;

ALTER FUNCTION "public"."refresh_materialized_view"("view_name" "text") OWNER TO "postgres";

CREATE FUNCTION "public"."update_chat"("chat_id" integer) RETURNS "void"
    LANGUAGE "plpgsql"
    AS $$
BEGIN
  UPDATE chats
  SET chat_count = chat_count + 2, updated_at = NOW()
  WHERE id = chat_id;
END;
$$;

ALTER FUNCTION "public"."update_chat"("chat_id" integer) OWNER TO "postgres";

SET default_tablespace = '';

SET default_table_access_method = "heap";

CREATE TABLE "public"."characters" (
    "created_at" timestamp with time zone DEFAULT "now"() NOT NULL,
    "updated_at" timestamp with time zone DEFAULT "now"() NOT NULL,
    "avatar" "text" NOT NULL,
    "name" character varying NOT NULL,
    "description" "text" NOT NULL,
    "first_message" "text" NOT NULL,
    "personality" "text" NOT NULL,
    "scenario" "text" NOT NULL,
    "example_dialogs" "text" NOT NULL,
    "is_public" boolean DEFAULT false NOT NULL,
    "creator_id" "uuid" NOT NULL,
    "id" "uuid" DEFAULT "extensions"."uuid_generate_v4"() NOT NULL,
    "is_nsfw" boolean DEFAULT false NOT NULL,
    "fts" "tsvector" GENERATED ALWAYS AS ((((("setweight"("to_tsvector"('"english"'::"regconfig", (COALESCE("name", ''::character varying))::"text"), 'A'::"char") || "setweight"("to_tsvector"('"english"'::"regconfig", COALESCE("description", ''::"text")), 'B'::"char")) || "setweight"("to_tsvector"('"english"'::"regconfig", COALESCE("personality", ''::"text")), 'C'::"char")) || "setweight"("to_tsvector"('"english"'::"regconfig", COALESCE("scenario", ''::"text")), 'C'::"char")) || "setweight"("to_tsvector"('"english"'::"regconfig", COALESCE("first_message", ''::"text")), 'D'::"char"))) STORED,
    "is_force_remove" boolean DEFAULT false NOT NULL
);

ALTER TABLE "public"."characters" OWNER TO "postgres";

CREATE TABLE "public"."chats" (
    "id" bigint NOT NULL,
    "created_at" timestamp with time zone DEFAULT "now"() NOT NULL,
    "character_id" "uuid" NOT NULL,
    "user_id" "uuid" NOT NULL,
    "is_public" boolean DEFAULT false NOT NULL,
    "summary" "text" DEFAULT ''::"text" NOT NULL,
    "summary_chat_id" bigint,
    "updated_at" timestamp with time zone DEFAULT "now"() NOT NULL,
    "chat_count" integer DEFAULT 0 NOT NULL
);

ALTER TABLE "public"."chats" OWNER TO "postgres";

CREATE MATERIALIZED VIEW "public"."character_stats" AS
 SELECT "ch"."id" AS "char_id",
    "count"("cts"."id") AS "total_chat",
    "sum"("cts"."chat_count") AS "total_message"
   FROM ("public"."characters" "ch"
     JOIN "public"."chats" "cts" ON (("ch"."id" = "cts"."character_id")))
  WHERE (("ch"."is_public" = true) AND ("cts"."chat_count" > 0))
  GROUP BY "ch"."id"
  WITH NO DATA;

ALTER TABLE "public"."character_stats" OWNER TO "postgres";

CREATE TABLE "public"."character_tags" (
    "tag_id" bigint NOT NULL,
    "character_id" "uuid" NOT NULL,
    "created_at" timestamp with time zone DEFAULT "now"()
);

ALTER TABLE "public"."character_tags" OWNER TO "postgres";

CREATE TABLE "public"."tags" (
    "id" bigint NOT NULL,
    "created_at" timestamp with time zone DEFAULT "now"() NOT NULL,
    "name" character varying NOT NULL,
    "slug" character varying NOT NULL,
    "description" "text" NOT NULL
);

ALTER TABLE "public"."tags" OWNER TO "postgres";

CREATE TABLE "public"."user_profiles" (
    "id" "uuid" NOT NULL,
    "name" character varying DEFAULT ''::character varying NOT NULL,
    "avatar" "text" DEFAULT ''::"text" NOT NULL,
    "about_me" "text" DEFAULT ''::"text" NOT NULL,
    "profile" "text" DEFAULT ''::"text" NOT NULL,
    "user_name" "text",
    "config" "json" DEFAULT '{}'::"json" NOT NULL,
    "is_verified" boolean DEFAULT false NOT NULL,
    "block_list" "json"
);

ALTER TABLE "public"."user_profiles" OWNER TO "postgres";

CREATE VIEW "public"."character_search" AS
 SELECT "characters"."created_at",
    "characters"."updated_at",
    "characters"."avatar",
    "characters"."name",
    "characters"."description",
    "characters"."first_message",
    "characters"."personality",
    "characters"."scenario",
    "characters"."example_dialogs",
    "characters"."is_public",
    "characters"."creator_id",
    "characters"."id",
    "characters"."is_nsfw",
    "characters"."fts",
    "characters"."is_force_remove",
    ARRAY( SELECT "tags"."id"
           FROM ("public"."character_tags"
             JOIN "public"."tags" ON (("character_tags"."tag_id" = "tags"."id")))
          WHERE ("character_tags"."character_id" = "characters"."id")) AS "tag_ids",
    COALESCE("user_profiles"."user_name", ("user_profiles"."name")::"text") AS "creator_name",
    "user_profiles"."is_verified" AS "creator_verified",
    COALESCE("character_stats"."total_chat", (0)::bigint) AS "total_chat",
    COALESCE("character_stats"."total_message", (0)::bigint) AS "total_message"
   FROM (("public"."characters"
     JOIN "public"."user_profiles" ON (("characters"."creator_id" = "user_profiles"."id")))
     LEFT JOIN "public"."character_stats" ON (("characters"."id" = "character_stats"."char_id")));

ALTER TABLE "public"."character_search" OWNER TO "postgres";

CREATE TABLE "public"."chat_messages" (
    "id" bigint NOT NULL,
    "created_at" timestamp with time zone DEFAULT "now"() NOT NULL,
    "chat_id" bigint NOT NULL,
    "is_bot" boolean DEFAULT false NOT NULL,
    "message" "text" NOT NULL,
    "is_main" boolean DEFAULT false NOT NULL
);

ALTER TABLE "public"."chat_messages" OWNER TO "postgres";


ALTER TABLE "public"."chat_messages" ALTER COLUMN "id" ADD GENERATED BY DEFAULT AS IDENTITY (
    SEQUENCE NAME "public"."chat_messages_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);

ALTER TABLE "public"."chats" ALTER COLUMN "id" ADD GENERATED BY DEFAULT AS IDENTITY (
    SEQUENCE NAME "public"."chats_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);

CREATE TABLE "public"."reviews" (
    "user_id" "uuid" NOT NULL,
    "character_id" "uuid" NOT NULL,
    "created_at" timestamp with time zone DEFAULT "now"() NOT NULL,
    "content" "text",
    "is_like" boolean DEFAULT true NOT NULL
);

ALTER TABLE "public"."reviews" OWNER TO "postgres";

ALTER TABLE "public"."tags" ALTER COLUMN "id" ADD GENERATED BY DEFAULT AS IDENTITY (
    SEQUENCE NAME "public"."tag_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);

CREATE TABLE "public"."user_reports" (
    "id" bigint NOT NULL,
    "url" "text" DEFAULT ''::"text" NOT NULL,
    "reason" "text" DEFAULT ''::"text" NOT NULL,
    "other" "text" DEFAULT ''::"text" NOT NULL,
    "created_at" timestamp with time zone DEFAULT "now"() NOT NULL,
    "profile_id" "uuid",
    "character_id" "uuid"
);

ALTER TABLE "public"."user_reports" OWNER TO "postgres";

ALTER TABLE "public"."user_reports" ALTER COLUMN "id" ADD GENERATED BY DEFAULT AS IDENTITY (
    SEQUENCE NAME "public"."user_reports_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);

ALTER TABLE ONLY "public"."character_tags"
    ADD CONSTRAINT "character_tags_pkey" PRIMARY KEY ("tag_id", "character_id");

ALTER TABLE ONLY "public"."characters"
    ADD CONSTRAINT "characters_pkey" PRIMARY KEY ("id");

ALTER TABLE ONLY "public"."chat_messages"
    ADD CONSTRAINT "chat_messages_id_key" UNIQUE ("id");

ALTER TABLE ONLY "public"."chat_messages"
    ADD CONSTRAINT "chat_messages_pkey" PRIMARY KEY ("id");

ALTER TABLE ONLY "public"."chats"
    ADD CONSTRAINT "chats_id_key" UNIQUE ("id");

ALTER TABLE ONLY "public"."chats"
    ADD CONSTRAINT "chats_pkey" PRIMARY KEY ("id");

ALTER TABLE ONLY "public"."reviews"
    ADD CONSTRAINT "reviews_pkey" PRIMARY KEY ("user_id", "character_id");

ALTER TABLE ONLY "public"."tags"
    ADD CONSTRAINT "tag_pkey" PRIMARY KEY ("id");

ALTER TABLE ONLY "public"."user_profiles"
    ADD CONSTRAINT "user_profiles_pkey" PRIMARY KEY ("id");

ALTER TABLE ONLY "public"."user_profiles"
    ADD CONSTRAINT "user_profiles_user_name_key" UNIQUE ("user_name");

ALTER TABLE ONLY "public"."user_reports"
    ADD CONSTRAINT "user_reports_pkey" PRIMARY KEY ("id");

CREATE UNIQUE INDEX "character_stats_char_id" ON "public"."character_stats" USING "btree" ("char_id");

CREATE INDEX "characters_fts" ON "public"."characters" USING "gin" ("fts");

CREATE INDEX "characters_is_public_created_date_idx" ON "public"."characters" USING "btree" ("is_public", "created_at" DESC);

CREATE INDEX "chat_messages_chat_id_created_date_idx" ON "public"."chat_messages" USING "btree" ("chat_id", "created_at" DESC);

ALTER TABLE ONLY "public"."character_tags"
    ADD CONSTRAINT "character_tags_character_id_fkey" FOREIGN KEY ("character_id") REFERENCES "public"."characters"("id") ON DELETE CASCADE;

ALTER TABLE ONLY "public"."character_tags"
    ADD CONSTRAINT "character_tags_tag_id_fkey" FOREIGN KEY ("tag_id") REFERENCES "public"."tags"("id") ON DELETE CASCADE;

ALTER TABLE ONLY "public"."characters"
    ADD CONSTRAINT "characters_creator_id_fkey" FOREIGN KEY ("creator_id") REFERENCES "public"."user_profiles"("id") ON DELETE CASCADE;

ALTER TABLE ONLY "public"."chat_messages"
    ADD CONSTRAINT "chat_messages_chat_id_fkey" FOREIGN KEY ("chat_id") REFERENCES "public"."chats"("id") ON DELETE CASCADE;

ALTER TABLE ONLY "public"."chats"
    ADD CONSTRAINT "chats_character_id_fkey" FOREIGN KEY ("character_id") REFERENCES "public"."characters"("id") ON DELETE CASCADE;

ALTER TABLE ONLY "public"."chats"
    ADD CONSTRAINT "chats_user_id_fkey" FOREIGN KEY ("user_id") REFERENCES "public"."user_profiles"("id") ON DELETE CASCADE;

ALTER TABLE ONLY "public"."reviews"
    ADD CONSTRAINT "reviews_character_id_fkey" FOREIGN KEY ("character_id") REFERENCES "public"."characters"("id") ON DELETE CASCADE;

ALTER TABLE ONLY "public"."reviews"
    ADD CONSTRAINT "reviews_user_id_fkey" FOREIGN KEY ("user_id") REFERENCES "public"."user_profiles"("id") ON DELETE CASCADE;

ALTER TABLE ONLY "public"."user_profiles"
    ADD CONSTRAINT "user_profiles_id_fkey" FOREIGN KEY ("id") REFERENCES "auth"."users"("id");

CREATE POLICY "Can delete your own character" ON "public"."characters" FOR DELETE TO "authenticated" USING (("auth"."uid"() = "creator_id"));

CREATE POLICY "Can delete your own chat" ON "public"."chats" FOR DELETE TO "authenticated" USING (("auth"."uid"() = "user_id"));

CREATE POLICY "Can view your own chat or public chat" ON "public"."chats" FOR SELECT USING (("is_public" OR ("auth"."uid"() = "user_id")));

CREATE POLICY "Enable delete for users based on id" ON "public"."user_profiles" FOR DELETE TO "authenticated" USING (("auth"."uid"() = "id"));

CREATE POLICY "Enable delete for users based on user_id" ON "public"."reviews" FOR DELETE TO "authenticated" USING (("auth"."uid"() = "user_id"));

CREATE POLICY "Enable read access for all users" ON "public"."character_tags" FOR SELECT USING (true);

CREATE POLICY "Enable read access for all users" ON "public"."characters" FOR SELECT USING (("is_public" OR ("auth"."uid"() = "creator_id")));

CREATE POLICY "Enable read access for all users" ON "public"."reviews" FOR SELECT USING (true);

CREATE POLICY "Enable read access for all users" ON "public"."tags" FOR SELECT USING (true);

CREATE POLICY "Enable read access owner only" ON "public"."user_profiles" FOR SELECT TO "authenticated" USING (("auth"."uid"() = "id"));

CREATE POLICY "Enable update for users based on user_id" ON "public"."reviews" FOR UPDATE TO "authenticated" USING (("auth"."uid"() = "user_id")) WITH CHECK (("auth"."uid"() = "user_id"));

CREATE POLICY "anon can create reports" ON "public"."user_reports" FOR INSERT WITH CHECK (true);

ALTER TABLE "public"."character_tags" ENABLE ROW LEVEL SECURITY;

ALTER TABLE "public"."characters" ENABLE ROW LEVEL SECURITY;

ALTER TABLE "public"."chat_messages" ENABLE ROW LEVEL SECURITY;

ALTER TABLE "public"."chats" ENABLE ROW LEVEL SECURITY;

ALTER TABLE "public"."reviews" ENABLE ROW LEVEL SECURITY;

ALTER TABLE "public"."tags" ENABLE ROW LEVEL SECURITY;

ALTER TABLE "public"."user_profiles" ENABLE ROW LEVEL SECURITY;

ALTER TABLE "public"."user_reports" ENABLE ROW LEVEL SECURITY;

REVOKE USAGE ON SCHEMA "public" FROM PUBLIC;
GRANT USAGE ON SCHEMA "public" TO "anon";
GRANT USAGE ON SCHEMA "public" TO "authenticated";
GRANT USAGE ON SCHEMA "public" TO "service_role";

GRANT ALL ON FUNCTION "public"."handle_new_user"() TO "anon";
GRANT ALL ON FUNCTION "public"."handle_new_user"() TO "authenticated";
GRANT ALL ON FUNCTION "public"."handle_new_user"() TO "service_role";

GRANT ALL ON FUNCTION "public"."pgrst_watch"() TO "anon";
GRANT ALL ON FUNCTION "public"."pgrst_watch"() TO "authenticated";
GRANT ALL ON FUNCTION "public"."pgrst_watch"() TO "service_role";

GRANT ALL ON FUNCTION "public"."refresh_materialized_view"("view_name" "text") TO "service_role";
GRANT ALL ON FUNCTION "public"."refresh_materialized_view"("view_name" "text") TO "dashboard_user";
GRANT ALL ON FUNCTION "public"."refresh_materialized_view"("view_name" "text") TO "supabase_admin";

GRANT ALL ON FUNCTION "public"."update_chat"("chat_id" integer) TO "service_role";
GRANT ALL ON FUNCTION "public"."update_chat"("chat_id" integer) TO "dashboard_user";
GRANT ALL ON FUNCTION "public"."update_chat"("chat_id" integer) TO "supabase_admin";

GRANT ALL ON TABLE "public"."characters" TO "anon";
GRANT ALL ON TABLE "public"."characters" TO "authenticated";
GRANT ALL ON TABLE "public"."characters" TO "service_role";

GRANT ALL ON TABLE "public"."chats" TO "anon";
GRANT ALL ON TABLE "public"."chats" TO "authenticated";
GRANT ALL ON TABLE "public"."chats" TO "service_role";

GRANT ALL ON TABLE "public"."character_stats" TO "anon";
GRANT ALL ON TABLE "public"."character_stats" TO "authenticated";
GRANT ALL ON TABLE "public"."character_stats" TO "service_role";

GRANT ALL ON TABLE "public"."character_tags" TO "anon";
GRANT ALL ON TABLE "public"."character_tags" TO "authenticated";
GRANT ALL ON TABLE "public"."character_tags" TO "service_role";

GRANT ALL ON TABLE "public"."tags" TO "anon";
GRANT ALL ON TABLE "public"."tags" TO "authenticated";
GRANT ALL ON TABLE "public"."tags" TO "service_role";

GRANT ALL ON TABLE "public"."user_profiles" TO "anon";
GRANT ALL ON TABLE "public"."user_profiles" TO "authenticated";
GRANT ALL ON TABLE "public"."user_profiles" TO "service_role";

GRANT ALL ON TABLE "public"."character_search" TO "anon";
GRANT ALL ON TABLE "public"."character_search" TO "authenticated";
GRANT ALL ON TABLE "public"."character_search" TO "service_role";

GRANT ALL ON TABLE "public"."chat_messages" TO "anon";
GRANT ALL ON TABLE "public"."chat_messages" TO "authenticated";
GRANT ALL ON TABLE "public"."chat_messages" TO "service_role";


GRANT ALL ON SEQUENCE "public"."chat_messages_id_seq" TO "anon";
GRANT ALL ON SEQUENCE "public"."chat_messages_id_seq" TO "authenticated";
GRANT ALL ON SEQUENCE "public"."chat_messages_id_seq" TO "service_role";

GRANT ALL ON SEQUENCE "public"."chats_id_seq" TO "anon";
GRANT ALL ON SEQUENCE "public"."chats_id_seq" TO "authenticated";
GRANT ALL ON SEQUENCE "public"."chats_id_seq" TO "service_role";

GRANT ALL ON TABLE "public"."reviews" TO "anon";
GRANT ALL ON TABLE "public"."reviews" TO "authenticated";
GRANT ALL ON TABLE "public"."reviews" TO "service_role";

GRANT ALL ON SEQUENCE "public"."tag_id_seq" TO "anon";
GRANT ALL ON SEQUENCE "public"."tag_id_seq" TO "authenticated";
GRANT ALL ON SEQUENCE "public"."tag_id_seq" TO "service_role";

GRANT ALL ON TABLE "public"."user_reports" TO "anon";
GRANT ALL ON TABLE "public"."user_reports" TO "authenticated";
GRANT ALL ON TABLE "public"."user_reports" TO "service_role";

GRANT ALL ON SEQUENCE "public"."user_reports_id_seq" TO "anon";
GRANT ALL ON SEQUENCE "public"."user_reports_id_seq" TO "authenticated";
GRANT ALL ON SEQUENCE "public"."user_reports_id_seq" TO "service_role";

ALTER DEFAULT PRIVILEGES FOR ROLE "postgres" IN SCHEMA "public" GRANT ALL ON SEQUENCES  TO "postgres";
ALTER DEFAULT PRIVILEGES FOR ROLE "postgres" IN SCHEMA "public" GRANT ALL ON SEQUENCES  TO "anon";
ALTER DEFAULT PRIVILEGES FOR ROLE "postgres" IN SCHEMA "public" GRANT ALL ON SEQUENCES  TO "authenticated";
ALTER DEFAULT PRIVILEGES FOR ROLE "postgres" IN SCHEMA "public" GRANT ALL ON SEQUENCES  TO "service_role";

ALTER DEFAULT PRIVILEGES FOR ROLE "postgres" IN SCHEMA "public" GRANT ALL ON FUNCTIONS  TO "postgres";
ALTER DEFAULT PRIVILEGES FOR ROLE "postgres" IN SCHEMA "public" GRANT ALL ON FUNCTIONS  TO "anon";
ALTER DEFAULT PRIVILEGES FOR ROLE "postgres" IN SCHEMA "public" GRANT ALL ON FUNCTIONS  TO "authenticated";
ALTER DEFAULT PRIVILEGES FOR ROLE "postgres" IN SCHEMA "public" GRANT ALL ON FUNCTIONS  TO "service_role";

ALTER DEFAULT PRIVILEGES FOR ROLE "postgres" IN SCHEMA "public" GRANT ALL ON TABLES  TO "postgres";
ALTER DEFAULT PRIVILEGES FOR ROLE "postgres" IN SCHEMA "public" GRANT ALL ON TABLES  TO "anon";
ALTER DEFAULT PRIVILEGES FOR ROLE "postgres" IN SCHEMA "public" GRANT ALL ON TABLES  TO "authenticated";
ALTER DEFAULT PRIVILEGES FOR ROLE "postgres" IN SCHEMA "public" GRANT ALL ON TABLES  TO "service_role";

RESET ALL;