-- trigger the function every time a user is created
create trigger on_auth_user_created
  after insert on auth.users
  for each row execute procedure public.handle_new_user();

ALTER TABLE IF EXISTS storage.objects DROP CONSTRAINT IF EXISTS objects_owner_fkey;

ALTER TABLE IF EXISTS storage.objects
    ADD CONSTRAINT objects_owner_fkey FOREIGN KEY (owner)
    REFERENCES auth.users (id) MATCH SIMPLE
    ON UPDATE NO ACTION
    ON DELETE SET NULL;

CREATE POLICY "Enable insert for authenticated users only"
    ON storage.objects
    AS PERMISSIVE
    FOR INSERT
    TO authenticated
    WITH CHECK (true);


CREATE POLICY "Owner can update their own object"
    ON storage.objects
    AS PERMISSIVE
    FOR UPDATE
    TO authenticated
    USING ((auth.uid() = owner))
    WITH CHECK ((auth.uid() = owner));


CREATE POLICY "Enable read access for to owner only"
    ON storage.objects
    AS PERMISSIVE
    FOR SELECT
    TO authenticated
    USING ((auth.uid() = owner));